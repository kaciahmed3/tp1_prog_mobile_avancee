import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'First Card'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: false,
      ),
      body: Container(
        alignment : Alignment.center,
          child:Stack(
            clipBehavior: Clip.none,
            children: <Widget>[

              Container(
                width: 350,
                height: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.pinkAccent,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget> [
                        Padding(
                          padding: const EdgeInsets.only(top:0.0),
                          child: Row(
                              children:const <Widget> [
                                Text("Kaci",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                  ),
                                ),
                                Text(" Ahmed",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ]
                          ),
                        ),

                        const Padding(padding:EdgeInsets.only(top:2.0),
                          child: Text("Kaciahmed3@gmail.com",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top:2.0),
                          child: Row(
                            children:const <Widget> [
                              Icon(Icons.person,
                                color: Colors.white,
                                size: 20,
                              ),
                              Text("Twitter: ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                              ),
                              Text("kaciAhmed ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                  bottom: 200,
                  left: 90.0,
                  child:Container(
                    width: 165,
                    height: 165,
                    decoration:BoxDecoration(
                      shape: BoxShape.circle,
                      image:  const DecorationImage(
                        image: AssetImage('assets/images/image_tp2_exo1.jpg'),
                        fit: BoxFit.fill,
                      ),
                      border: Border.all(
                        color: Colors.pinkAccent,
                        width: 4,
                      ),
                    ),
                  )
              )
              ,
            ],
          ),
      )
    );
  }
}
